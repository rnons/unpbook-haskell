import           Control.Applicative ((<$>))
import           Control.Monad (forever)
import qualified Data.ByteString.Char8 as B
import           Data.Time (formatTime, defaultTimeLocale, getZonedTime)
import           Network.Socket
import           Network.Socket.ByteString (sendAll)

main :: IO ()
main = withSocketsDo $ do
    addr <- head <$> getAddrInfo (Just (defaultHints {addrFlags = [AI_PASSIVE]}))
                                 Nothing (Just "13")
    sock <- socket (addrFamily addr) Stream defaultProtocol
    bindSocket sock (addrAddress addr)
    listen sock 128
    serveForever sock
  where
    serveForever s = forever $ do
        (conn, _) <- accept s
        handler conn
    format = formatTime defaultTimeLocale "%a %b %e %H:%M:%S %Y %n"
    handler conn = do
        getZonedTime >>= sendAll conn . B.pack . format
        close conn
