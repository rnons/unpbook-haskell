import           Control.Applicative ((<$>))
import           Control.Monad (unless)
import qualified Data.ByteString as B
import           Network.Socket hiding (recv)
import           Network.Socket.ByteString (recv)
import           System.Environment (getArgs)

main :: IO ()
main = withSocketsDo $ do
    args <- getArgs
    let server = if not (null args) then head args
        else error "usage: runhaskell daytimetcpcli.hs <IPaddress>"

    addr <- head <$> getAddrInfo Nothing (Just server) (Just "13")
    sock <- socket (addrFamily addr) Stream defaultProtocol
    connect sock (addrAddress addr)
    handler sock
  where
    maxline = 4096
    handler s = do
        line <- recv s maxline
        unless (B.null line) $ B.putStr line >> handler s
